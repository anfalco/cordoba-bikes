var map = L.map('main_map').setView([-31.4228733,-64.1833163],15);




L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
/*
L.marker([-25.2719,-57.5399]).addTo(map)
L.marker([-25.269323, -57.537675]).addTo(map);
L.marker([-25.269323, -57.535675]).addTo(map);
*/

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            var desc_bici='Bicicleta ' +  bici.id + '\n' 
            + 'Modelo ' + bici.modelo + '\n' 
            + 'Color ' + bici.color;
            L.marker(bici.ubicacion,{title: desc_bici}).addTo(map);
        });        
    }
})